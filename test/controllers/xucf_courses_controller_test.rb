require 'test_helper'

class XucfCoursesControllerTest < ActionController::TestCase
  setup do
    @xucf_course = xucf_courses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:xucf_courses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create xucf_course" do
    assert_difference('XucfCourse.count') do
      post :create, xucf_course: { xcf_credit: @xucf_course.xcf_credit, xcf_exam: @xucf_course.xcf_exam, xcf_hour: @xucf_course.xcf_hour, xcf_name: @xucf_course.xcf_name, xcf_no: @xucf_course.xcf_no }
    end

    assert_redirected_to xucf_course_path(assigns(:xucf_course))
  end

  test "should show xucf_course" do
    get :show, id: @xucf_course
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @xucf_course
    assert_response :success
  end

  test "should update xucf_course" do
    patch :update, id: @xucf_course, xucf_course: { xcf_credit: @xucf_course.xcf_credit, xcf_exam: @xucf_course.xcf_exam, xcf_hour: @xucf_course.xcf_hour, xcf_name: @xucf_course.xcf_name, xcf_no: @xucf_course.xcf_no }
    assert_redirected_to xucf_course_path(assigns(:xucf_course))
  end

  test "should destroy xucf_course" do
    assert_difference('XucfCourse.count', -1) do
      delete :destroy, id: @xucf_course
    end

    assert_redirected_to xucf_courses_path
  end
end
