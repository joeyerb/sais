require 'test_helper'

class XucfMajorsControllerTest < ActionController::TestCase
  setup do
    @xucf_major = xucf_majors(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:xucf_majors)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create xucf_major" do
    assert_difference('XucfMajor.count') do
      post :create, xucf_major: { xcf_college_id: @xucf_major.xcf_college_id, xcf_name: @xucf_major.xcf_name }
    end

    assert_redirected_to xucf_major_path(assigns(:xucf_major))
  end

  test "should show xucf_major" do
    get :show, id: @xucf_major
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @xucf_major
    assert_response :success
  end

  test "should update xucf_major" do
    patch :update, id: @xucf_major, xucf_major: { xcf_college_id: @xucf_major.xcf_college_id, xcf_name: @xucf_major.xcf_name }
    assert_redirected_to xucf_major_path(assigns(:xucf_major))
  end

  test "should destroy xucf_major" do
    assert_difference('XucfMajor.count', -1) do
      delete :destroy, id: @xucf_major
    end

    assert_redirected_to xucf_majors_path
  end
end
