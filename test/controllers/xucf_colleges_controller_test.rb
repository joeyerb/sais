require 'test_helper'

class XucfCollegesControllerTest < ActionController::TestCase
  setup do
    @xucf_college = xucf_colleges(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:xucf_colleges)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create xucf_college" do
    assert_difference('XucfCollege.count') do
      post :create, xucf_college: { xcf_name: @xucf_college.xcf_name }
    end

    assert_redirected_to xucf_college_path(assigns(:xucf_college))
  end

  test "should show xucf_college" do
    get :show, id: @xucf_college
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @xucf_college
    assert_response :success
  end

  test "should update xucf_college" do
    patch :update, id: @xucf_college, xucf_college: { xcf_name: @xucf_college.xcf_name }
    assert_redirected_to xucf_college_path(assigns(:xucf_college))
  end

  test "should destroy xucf_college" do
    assert_difference('XucfCollege.count', -1) do
      delete :destroy, id: @xucf_college
    end

    assert_redirected_to xucf_colleges_path
  end
end
