require 'test_helper'

class XucfClassesControllerTest < ActionController::TestCase
  setup do
    @xucf_class = xucf_classes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:xucf_classes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create xucf_class" do
    assert_difference('XucfClass.count') do
      post :create, xucf_class: { xcf_grade: @xucf_class.xcf_grade, xcf_major_id: @xucf_class.xcf_major_id, xcf_no: @xucf_class.xcf_no }
    end

    assert_redirected_to xucf_class_path(assigns(:xucf_class))
  end

  test "should show xucf_class" do
    get :show, id: @xucf_class
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @xucf_class
    assert_response :success
  end

  test "should update xucf_class" do
    patch :update, id: @xucf_class, xucf_class: { xcf_grade: @xucf_class.xcf_grade, xcf_major_id: @xucf_class.xcf_major_id, xcf_no: @xucf_class.xcf_no }
    assert_redirected_to xucf_class_path(assigns(:xucf_class))
  end

  test "should destroy xucf_class" do
    assert_difference('XucfClass.count', -1) do
      delete :destroy, id: @xucf_class
    end

    assert_redirected_to xucf_classes_path
  end
end
