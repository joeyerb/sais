require 'test_helper'

class XucfReportsControllerTest < ActionController::TestCase
  setup do
    @xucf_report = xucf_reports(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:xucf_reports)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create xucf_report" do
    assert_difference('XucfReport.count') do
      post :create, xucf_report: { xcf_course_id: @xucf_report.xcf_course_id, xcf_score: @xucf_report.xcf_score, xcf_student_id: @xucf_report.xcf_student_id }
    end

    assert_redirected_to xucf_report_path(assigns(:xucf_report))
  end

  test "should show xucf_report" do
    get :show, id: @xucf_report
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @xucf_report
    assert_response :success
  end

  test "should update xucf_report" do
    patch :update, id: @xucf_report, xucf_report: { xcf_course_id: @xucf_report.xcf_course_id, xcf_score: @xucf_report.xcf_score, xcf_student_id: @xucf_report.xcf_student_id }
    assert_redirected_to xucf_report_path(assigns(:xucf_report))
  end

  test "should destroy xucf_report" do
    assert_difference('XucfReport.count', -1) do
      delete :destroy, id: @xucf_report
    end

    assert_redirected_to xucf_reports_path
  end
end
