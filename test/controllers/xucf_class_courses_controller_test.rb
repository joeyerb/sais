require 'test_helper'

class XucfClassCoursesControllerTest < ActionController::TestCase
  setup do
    @xucf_class_course = xucf_class_courses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:xucf_class_courses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create xucf_class_course" do
    assert_difference('XucfClassCourse.count') do
      post :create, xucf_class_course: { xcf_class_id: @xucf_class_course.xcf_class_id, xcf_course_id: @xucf_class_course.xcf_course_id, xcf_teacher_id: @xucf_class_course.xcf_teacher_id, xcf_term: @xucf_class_course.xcf_term }
    end

    assert_redirected_to xucf_class_course_path(assigns(:xucf_class_course))
  end

  test "should show xucf_class_course" do
    get :show, id: @xucf_class_course
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @xucf_class_course
    assert_response :success
  end

  test "should update xucf_class_course" do
    patch :update, id: @xucf_class_course, xucf_class_course: { xcf_class_id: @xucf_class_course.xcf_class_id, xcf_course_id: @xucf_class_course.xcf_course_id, xcf_teacher_id: @xucf_class_course.xcf_teacher_id, xcf_term: @xucf_class_course.xcf_term }
    assert_redirected_to xucf_class_course_path(assigns(:xucf_class_course))
  end

  test "should destroy xucf_class_course" do
    assert_difference('XucfClassCourse.count', -1) do
      delete :destroy, id: @xucf_class_course
    end

    assert_redirected_to xucf_class_courses_path
  end
end
