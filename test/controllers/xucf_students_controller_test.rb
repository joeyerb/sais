require 'test_helper'

class XucfStudentsControllerTest < ActionController::TestCase
  setup do
    @xucf_student = xucf_students(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:xucf_students)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create xucf_student" do
    assert_difference('XucfStudent.count') do
      post :create, xucf_student: { xcf_birthplace: @xucf_student.xcf_birthplace, xcf_birthyear: @xucf_student.xcf_birthyear, xcf_class_id: @xucf_student.xcf_class_id, xcf_credit: @xucf_student.xcf_credit, xcf_gender: @xucf_student.xcf_gender, xcf_name: @xucf_student.xcf_name, xcf_no: @xucf_student.xcf_no }
    end

    assert_redirected_to xucf_student_path(assigns(:xucf_student))
  end

  test "should show xucf_student" do
    get :show, id: @xucf_student
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @xucf_student
    assert_response :success
  end

  test "should update xucf_student" do
    patch :update, id: @xucf_student, xucf_student: { xcf_birthplace: @xucf_student.xcf_birthplace, xcf_birthyear: @xucf_student.xcf_birthyear, xcf_class_id: @xucf_student.xcf_class_id, xcf_credit: @xucf_student.xcf_credit, xcf_gender: @xucf_student.xcf_gender, xcf_name: @xucf_student.xcf_name, xcf_no: @xucf_student.xcf_no }
    assert_redirected_to xucf_student_path(assigns(:xucf_student))
  end

  test "should destroy xucf_student" do
    assert_difference('XucfStudent.count', -1) do
      delete :destroy, id: @xucf_student
    end

    assert_redirected_to xucf_students_path
  end
end
