require 'test_helper'

class XucfTeachersControllerTest < ActionController::TestCase
  setup do
    @xucf_teacher = xucf_teachers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:xucf_teachers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create xucf_teacher" do
    assert_difference('XucfTeacher.count') do
      post :create, xucf_teacher: { xcf_birthyear: @xucf_teacher.xcf_birthyear, xcf_college_id: @xucf_teacher.xcf_college_id, xcf_gender: @xucf_teacher.xcf_gender, xcf_name: @xucf_teacher.xcf_name, xcf_no: @xucf_teacher.xcf_no, xcf_tel: @xucf_teacher.xcf_tel, xcf_title: @xucf_teacher.xcf_title }
    end

    assert_redirected_to xucf_teacher_path(assigns(:xucf_teacher))
  end

  test "should show xucf_teacher" do
    get :show, id: @xucf_teacher
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @xucf_teacher
    assert_response :success
  end

  test "should update xucf_teacher" do
    patch :update, id: @xucf_teacher, xucf_teacher: { xcf_birthyear: @xucf_teacher.xcf_birthyear, xcf_college_id: @xucf_teacher.xcf_college_id, xcf_gender: @xucf_teacher.xcf_gender, xcf_name: @xucf_teacher.xcf_name, xcf_no: @xucf_teacher.xcf_no, xcf_tel: @xucf_teacher.xcf_tel, xcf_title: @xucf_teacher.xcf_title }
    assert_redirected_to xucf_teacher_path(assigns(:xucf_teacher))
  end

  test "should destroy xucf_teacher" do
    assert_difference('XucfTeacher.count', -1) do
      delete :destroy, id: @xucf_teacher
    end

    assert_redirected_to xucf_teachers_path
  end
end
