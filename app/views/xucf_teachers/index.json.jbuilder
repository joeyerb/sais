json.array!(@xucf_teachers) do |xucf_teacher|
  json.extract! xucf_teacher, :id, :xcf_no, :xcf_name, :xucf_college_id, :xcf_gender, :xcf_birthyear, :xcf_title, :xcf_tel
  json.url xucf_teacher_url(xucf_teacher, format: :json)
end
