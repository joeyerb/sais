json.array!(@xucf_courses) do |xucf_course|
  json.extract! xucf_course, :id, :xcf_no, :xcf_name, :xcf_hour, :xcf_exam, :xcf_credit
  json.url xucf_course_url(xucf_course, format: :json)
end
