json.array!(@xucf_classes) do |xucf_class|
  json.extract! xucf_class, :id, :xcf_no, :xucf_major_id, :xcf_grade
  json.url xucf_class_url(xucf_class, format: :json)
end
