json.array!(@xucf_majors) do |xucf_major|
  json.extract! xucf_major, :id, :xcf_name, :xucf_college_id
  json.url xucf_major_url(xucf_major, format: :json)
end
