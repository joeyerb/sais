json.array!(@xucf_colleges) do |xucf_college|
  json.extract! xucf_college, :id, :xcf_name
  json.url xucf_college_url(xucf_college, format: :json)
end
