json.array!(@xucf_reports) do |xucf_report|
  json.extract! xucf_report, :id, :xucf_student_id, :xucf_course_id, :xcf_score
  json.url xucf_report_url(xucf_report, format: :json)
end
