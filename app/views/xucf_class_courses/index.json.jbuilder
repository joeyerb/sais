json.array!(@xucf_class_courses) do |xucf_class_course|
  json.extract! xucf_class_course, :id, :xucf_class_id, :xucf_course_id, :xucf_teacher_id, :xcf_term
  json.url xucf_class_course_url(xucf_class_course, format: :json)
end
