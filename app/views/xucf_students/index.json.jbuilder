json.array!(@xucf_students) do |xucf_student|
  json.extract! xucf_student, :id, :xcf_no, :xcf_name, :xucf_class_id, :xcf_gender, :xcf_birthyear, :xcf_birthplace, :xcf_credit
  json.url xucf_student_url(xucf_student, format: :json)
end
