class XucfCourse < ActiveRecord::Base
  has_many :xucf_class_courses
  has_many :xucf_classes, :through => :xucf_class_courses
  has_many :xucf_reports
  has_many :xucf_students, :through => :xucf_reports
end
