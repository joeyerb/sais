class XucfStudent < ActiveRecord::Base
  belongs_to :xucf_class
  has_many :xucf_reports
  has_many :xucf_courses, :through => :xucf_reports
end
