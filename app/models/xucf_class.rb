class XucfClass < ActiveRecord::Base
  belongs_to :xucf_major
  has_many :xucf_students
  has_many :xucf_class_courses
  has_many :xucf_courses, :through => :xucf_class_courses
end
