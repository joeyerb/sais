class XucfTeachersController < ApplicationController
  before_action :set_xucf_teacher, only: [:show, :edit, :update, :destroy]

  def courses
    courses = XucfClassCourse.where( :xucf_teacher_id => params[:id] )
    @named_courses = []
    courses.each do |ccourse|
      course = XucfCourse.where( :id => ccourse[:xucf_course_id] ).first
      klass = XucfClass.where( :id => ccourse[:xucf_class_id] ).first
      @named_courses << { course_name: course[:xcf_name], class_no: klass[:xcf_no], term: ccourse[:xcf_term] }
    end
    respond_to do |format|
      format.json { render :json => @named_courses }
    end
  end

  # GET /xucf_teachers
  # GET /xucf_teachers.json
  def index
    @xucf_teachers = XucfTeacher.all
  end

  # GET /xucf_teachers/1
  # GET /xucf_teachers/1.json
  def show
  end

  # GET /xucf_teachers/new
  def new
    @xucf_teacher = XucfTeacher.new
  end

  # GET /xucf_teachers/1/edit
  def edit
  end

  # POST /xucf_teachers
  # POST /xucf_teachers.json
  def create
    @xucf_teacher = XucfTeacher.new(xucf_teacher_params)

    respond_to do |format|
      if @xucf_teacher.save
        format.html { redirect_to @xucf_teacher, notice: 'Xucf teacher was successfully created.' }
        format.json { render :show, status: :created, location: @xucf_teacher }
      else
        format.html { render :new }
        format.json { render json: @xucf_teacher.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /xucf_teachers/1
  # PATCH/PUT /xucf_teachers/1.json
  def update
    respond_to do |format|
      if @xucf_teacher.update(xucf_teacher_params)
        format.html { redirect_to @xucf_teacher, notice: 'Xucf teacher was successfully updated.' }
        format.json { render :show, status: :ok, location: @xucf_teacher }
      else
        format.html { render :edit }
        format.json { render json: @xucf_teacher.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /xucf_teachers/1
  # DELETE /xucf_teachers/1.json
  def destroy
    @xucf_teacher.destroy
    respond_to do |format|
      format.html { redirect_to xucf_teachers_url, notice: 'Xucf teacher was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_xucf_teacher
      @xucf_teacher = XucfTeacher.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def xucf_teacher_params
      params.require(:xucf_teacher).permit(:xcf_no, :xcf_name, :xucf_college_id, :xcf_gender, :xcf_birthyear, :xcf_title, :xcf_tel)
    end
end
