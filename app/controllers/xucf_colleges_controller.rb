class XucfCollegesController < ApplicationController
  before_action :set_xucf_college, only: [:show, :edit, :update, :destroy]

  # GET /xucf_colleges
  # GET /xucf_colleges.json
  def index
    @xucf_colleges = XucfCollege.all
  end

  # GET /xucf_colleges/1
  # GET /xucf_colleges/1.json
  def show
  end

  # GET /xucf_colleges/new
  def new
    @xucf_college = XucfCollege.new
  end

  # GET /xucf_colleges/1/edit
  def edit
  end

  # POST /xucf_colleges
  # POST /xucf_colleges.json
  def create
    @xucf_college = XucfCollege.new(xucf_college_params)

    respond_to do |format|
      if @xucf_college.save
        format.html { redirect_to @xucf_college, notice: 'Xucf college was successfully created.' }
        format.json { render :show, status: :created, location: @xucf_college }
      else
        format.html { render :new }
        format.json { render json: @xucf_college.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /xucf_colleges/1
  # PATCH/PUT /xucf_colleges/1.json
  def update
    respond_to do |format|
      if @xucf_college.update(xucf_college_params)
        format.html { redirect_to @xucf_college, notice: 'Xucf college was successfully updated.' }
        format.json { render :show, status: :ok, location: @xucf_college }
      else
        format.html { render :edit }
        format.json { render json: @xucf_college.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /xucf_colleges/1
  # DELETE /xucf_colleges/1.json
  def destroy
    @xucf_college.destroy
    respond_to do |format|
      format.html { redirect_to xucf_colleges_url, notice: 'Xucf college was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_xucf_college
      @xucf_college = XucfCollege.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def xucf_college_params
      params.require(:xucf_college).permit(:xcf_name)
    end
end
