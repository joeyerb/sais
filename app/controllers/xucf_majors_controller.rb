class XucfMajorsController < ApplicationController
  before_action :set_xucf_major, only: [:show, :edit, :update, :destroy]

  def rank
    classes = XucfClass.where( :xucf_major => params[:id], :xcf_grade => params[:grade_id] )
    class_ids = []
    classes.each do |klass|
      class_ids << klass[:id]
    end
    @students = XucfStudent.where(:xucf_class_id => class_ids).order("xcf_credit DESC")
    respond_to do |format|
      format.json { render :json => @students }
    end
  end

  # GET /xucf_majors
  # GET /xucf_majors.json
  def index
    @xucf_majors = XucfMajor.all
  end

  # GET /xucf_majors/1
  # GET /xucf_majors/1.json
  def show
  end

  # GET /xucf_majors/new
  def new
    @xucf_major = XucfMajor.new
  end

  # GET /xucf_majors/1/edit
  def edit
  end

  # POST /xucf_majors
  # POST /xucf_majors.json
  def create
    @xucf_major = XucfMajor.new(xucf_major_params)

    respond_to do |format|
      if @xucf_major.save
        format.html { redirect_to @xucf_major, notice: 'Xucf major was successfully created.' }
        format.json { render :show, status: :created, location: @xucf_major }
      else
        format.html { render :new }
        format.json { render json: @xucf_major.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /xucf_majors/1
  # PATCH/PUT /xucf_majors/1.json
  def update
    respond_to do |format|
      if @xucf_major.update(xucf_major_params)
        format.html { redirect_to @xucf_major, notice: 'Xucf major was successfully updated.' }
        format.json { render :show, status: :ok, location: @xucf_major }
      else
        format.html { render :edit }
        format.json { render json: @xucf_major.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /xucf_majors/1
  # DELETE /xucf_majors/1.json
  def destroy
    @xucf_major.destroy
    respond_to do |format|
      format.html { redirect_to xucf_majors_url, notice: 'Xucf major was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_xucf_major
      @xucf_major = XucfMajor.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def xucf_major_params
      params.require(:xucf_major).permit(:xcf_name, :xucf_college_id)
    end
end
