class XucfReportsController < ApplicationController
  skip_before_filter :verify_authenticity_token
  before_action :set_xucf_report, only: [:show, :edit, :update, :destroy]

  # GET /xucf_reports
  # GET /xucf_reports.json
  def index
    @xucf_reports = XucfReport.all
  end

  # GET /xucf_reports/1
  # GET /xucf_reports/1.json
  def show
  end

  # GET /xucf_reports/new
  def new
    @xucf_report = XucfReport.new
  end

  # GET /xucf_reports/1/edit
  def edit
  end

  # POST /xucf_reports
  # POST /xucf_reports.json
  def create
    @xucf_report = XucfReport.new(xucf_report_params)

    respond_to do |format|
      if @xucf_report.save
        format.html { redirect_to @xucf_report, notice: 'Xucf report was successfully created.' }
        format.json { render :show, status: :created, location: @xucf_report }
      else
        format.html { render :new }
        format.json { render json: @xucf_report.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /xucf_reports/1
  # PATCH/PUT /xucf_reports/1.json
  def update
    respond_to do |format|
      if @xucf_report.update(xucf_report_params)
        format.html { redirect_to @xucf_report, notice: 'Xucf report was successfully updated.' }
        format.json { render :show, status: :ok, location: @xucf_report }
      else
        format.html { render :edit }
        format.json { render json: @xucf_report.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /xucf_reports/1
  # DELETE /xucf_reports/1.json
  def destroy
    @xucf_report.destroy
    respond_to do |format|
      format.html { redirect_to xucf_reports_url, notice: 'Xucf report was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_xucf_report
      @xucf_report = XucfReport.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def xucf_report_params
      params.require(:xucf_report).permit(:xucf_student_id, :xucf_course_id, :xcf_score)
    end
end
