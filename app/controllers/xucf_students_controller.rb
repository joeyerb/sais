class XucfStudentsController < ApplicationController
  before_action :set_xucf_student, only: [:show, :edit, :update, :destroy]
  before_action :find_class

  def courses
    @courses = XucfCourse.joins( :xucf_reports ).where( "xucf_reports.xucf_student_id = ?", params[:id] )
    respond_to do |format|
      format.json { render :json => @courses }
    end
  end

  def scores
    reports = XucfReport.where( :xucf_student_id => params[:id] )
    @named_reports = []
    reports.each do |report|
      course = XucfCourse.find(report[:xucf_course_id])
      @named_reports << { course_name: course[:xcf_name], credit: course[:xcf_credit], score: report[:xcf_score] }
    end
    respond_to do |format|
      format.json { render :json => @named_reports }
    end
  end

  def term
    courses = XucfClassCourse.where( :xcf_term => params[:term_id], :xucf_class_id => params[:xucf_class_id] )
    term_reports = []
    courses.each do |course|
      course_detail = XucfCourse.find(course[:id])
      report = XucfReport.where( :xucf_student_id => params[:id], :xucf_course_id => course[:id] ).first
      if report
        term_reports << { course_name: course_detail[:xcf_name], credit: course_detail[:xcf_credit], score: report[:xcf_score] }
      else
        term_reports << { course_name: course_detail[:xcf_name], credit: course_detail[:xcf_credit], score: 0 }
      end
    end
    respond_to do |format|
      format.json { render :json => term_reports }
    end

  end

  # GET /xucf_students
  # GET /xucf_students.json
  def index
    @xucf_students = XucfStudent.all
  end

  # GET /xucf_students/1
  # GET /xucf_students/1.json
  def show
  end

  # GET /xucf_students/new
  def new
    @xucf_student = XucfStudent.new
  end

  # GET /xucf_students/1/edit
  def edit
  end

  # POST /xucf_students
  # POST /xucf_students.json
  def create
    @xucf_student = XucfStudent.new(xucf_student_params)

    respond_to do |format|
      if @xucf_student.save
        format.html { redirect_to @xucf_student, notice: 'Xucf student was successfully created.' }
        format.json { render :show, status: :created, location: @xucf_student }
      else
        format.html { render :new }
        format.json { render json: @xucf_student.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /xucf_students/1
  # PATCH/PUT /xucf_students/1.json
  def update
    respond_to do |format|
      if @xucf_student.update(xucf_student_params)
        format.html { redirect_to xucf_class_xucf_student_path(@xucf_class, @xucf_student), notice: 'Student info was successfully updated.' }
        format.json { render :show, status: :ok, location: @xucf_student }
      else
        format.html { render :edit }
        format.json { render json: @xucf_student.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /xucf_students/1
  # DELETE /xucf_students/1.json
  def destroy
    @xucf_student.destroy
    respond_to do |format|
      format.html { redirect_to xucf_students_url, notice: 'Xucf student was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_xucf_student
      @xucf_student = XucfStudent.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def xucf_student_params
      params.require(:xucf_student).permit(:xcf_no, :xcf_name, :xucf_class_id, :xcf_gender, :xcf_birthyear, :xcf_birthplace, :xcf_credit)
    end

    def find_class
      @xucf_class = XucfClass.find(params[:xucf_class_id])
    end

end
