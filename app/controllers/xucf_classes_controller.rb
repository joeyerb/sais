class XucfClassesController < ApplicationController
  before_action :set_xucf_class, only: [:show, :edit, :update, :destroy]

  def courses
    class_courses = XucfClassCourse.where( :xucf_class_id => params[:id] )
    @named_courses = []
    class_courses.each do |ccourse|
      course = XucfCourse.where( :id => ccourse[:xucf_course_id] ).first
      teacher = XucfTeacher.where( :id => ccourse[:xucf_teacher_id] ).first
      @named_courses << { course_name: course[:xcf_name], teacher_name: teacher[:xcf_name], term: ccourse[:xcf_term] }
    end
    respond_to do |format|
      format.json { render :json => @named_courses }
    end
  end

  # GET /xucf_classes
  # GET /xucf_classes.json
  def index
    @xucf_classes = XucfClass.all
  end

  # GET /xucf_classes/1
  # GET /xucf_classes/1.json
  def show
  end

  # GET /xucf_classes/new
  def new
    @xucf_class = XucfClass.new
  end

  # GET /xucf_classes/1/edit
  def edit
  end

  # POST /xucf_classes
  # POST /xucf_classes.json
  def create
    @xucf_class = XucfClass.new(xucf_class_params)

    respond_to do |format|
      if @xucf_class.save
        format.html { redirect_to @xucf_class, notice: 'Xucf class was successfully created.' }
        format.json { render :show, status: :created, location: @xucf_class }
      else
        format.html { render :new }
        format.json { render json: @xucf_class.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /xucf_classes/1
  # PATCH/PUT /xucf_classes/1.json
  def update
    respond_to do |format|
      if @xucf_class.update(xucf_class_params)
        format.html { redirect_to @xucf_class, notice: 'Xucf class was successfully updated.' }
        format.json { render :show, status: :ok, location: @xucf_class }
      else
        format.html { render :edit }
        format.json { render json: @xucf_class.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /xucf_classes/1
  # DELETE /xucf_classes/1.json
  def destroy
    @xucf_class.destroy
    respond_to do |format|
      format.html { redirect_to xucf_classes_url, notice: 'Xucf class was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_xucf_class
      @xucf_class = XucfClass.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def xucf_class_params
      params.require(:xucf_class).permit(:xcf_no, :xucf_major_id, :xcf_grade)
    end
end
