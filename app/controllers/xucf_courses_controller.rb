class XucfCoursesController < ApplicationController
  before_action :set_xucf_course, only: [:show, :edit, :update, :destroy]

  def grade
    classes = XucfClass.where( :xcf_grade => params[:grade_id] )
    students = []
    classes.each do |klass|
      students += XucfStudent.where( :xucf_class_id => klass[:id] )
    end
    reports = []
    students.each do |student|
      report = XucfReport.where( :xucf_student_id => student[:id]).first
      report || next
      reports << { class_no: student[:xucf_class_id], student_name: student[:xcf_name], score: report[:xcf_score] }
    end

    respond_to do |format|
      format.json { render :json => reports }
    end
  end


  # GET /xucf_courses
  # GET /xucf_courses.json
  def index
    @xucf_courses = XucfCourse.all
  end

  # GET /xucf_courses/1
  # GET /xucf_courses/1.json
  def show
  end

  # GET /xucf_courses/new
  def new
    @xucf_course = XucfCourse.new
  end

  # GET /xucf_courses/1/edit
  def edit
  end

  # POST /xucf_courses
  # POST /xucf_courses.json
  def create
    @xucf_course = XucfCourse.new(xucf_course_params)

    respond_to do |format|
      if @xucf_course.save
        format.html { redirect_to @xucf_course, notice: 'Xucf course was successfully created.' }
        format.json { render :show, status: :created, location: @xucf_course }
      else
        format.html { render :new }
        format.json { render json: @xucf_course.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /xucf_courses/1
  # PATCH/PUT /xucf_courses/1.json
  def update
    respond_to do |format|
      if @xucf_course.update(xucf_course_params)
        format.html { redirect_to @xucf_course, notice: 'Xucf course was successfully updated.' }
        format.json { render :show, status: :ok, location: @xucf_course }
      else
        format.html { render :edit }
        format.json { render json: @xucf_course.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /xucf_courses/1
  # DELETE /xucf_courses/1.json
  def destroy
    @xucf_course.destroy
    respond_to do |format|
      format.html { redirect_to xucf_courses_url, notice: 'Xucf course was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_xucf_course
      @xucf_course = XucfCourse.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def xucf_course_params
      params.require(:xucf_course).permit(:xcf_no, :xcf_name, :xcf_hour, :xcf_exam, :xcf_credit)
    end
end
