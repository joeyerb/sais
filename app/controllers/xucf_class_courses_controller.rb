class XucfClassCoursesController < ApplicationController
  before_action :set_xucf_class_course, only: [:show, :edit, :update, :destroy]

  # GET /xucf_class_courses
  # GET /xucf_class_courses.json
  def index
    @xucf_class_courses = XucfClassCourse.all
  end

  # GET /xucf_class_courses/1
  # GET /xucf_class_courses/1.json
  def show
  end

  # GET /xucf_class_courses/new
  def new
    @xucf_class_course = XucfClassCourse.new
  end

  # GET /xucf_class_courses/1/edit
  def edit
  end

  # POST /xucf_class_courses
  # POST /xucf_class_courses.json
  def create
    @xucf_class_course = XucfClassCourse.new(xucf_class_course_params)

    respond_to do |format|
      if @xucf_class_course.save
        format.html { redirect_to @xucf_class_course, notice: 'Xucf class course was successfully created.' }
        format.json { render :show, status: :created, location: @xucf_class_course }
      else
        format.html { render :new }
        format.json { render json: @xucf_class_course.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /xucf_class_courses/1
  # PATCH/PUT /xucf_class_courses/1.json
  def update
    respond_to do |format|
      if @xucf_class_course.update(xucf_class_course_params)
        format.html { redirect_to @xucf_class_course, notice: 'Xucf class course was successfully updated.' }
        format.json { render :show, status: :ok, location: @xucf_class_course }
      else
        format.html { render :edit }
        format.json { render json: @xucf_class_course.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /xucf_class_courses/1
  # DELETE /xucf_class_courses/1.json
  def destroy
    @xucf_class_course.destroy
    respond_to do |format|
      format.html { redirect_to xucf_class_courses_url, notice: 'Xucf class course was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_xucf_class_course
      @xucf_class_course = XucfClassCourse.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def xucf_class_course_params
      params.require(:xucf_class_course).permit(:xucf_class_id, :xucf_course_id, :xucf_teacher_id, :xcf_term)
    end
end
