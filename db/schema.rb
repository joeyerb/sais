# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150715031541) do

  create_table "xucf_class_courses", force: :cascade do |t|
    t.integer  "xucf_class_id",   limit: 4,    null: false
    t.integer  "xucf_course_id",  limit: 4,    null: false
    t.integer  "xucf_teacher_id", limit: 4
    t.string   "xcf_term",        limit: 4000
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "xucf_classes", force: :cascade do |t|
    t.string   "xcf_no",        limit: 4000
    t.integer  "xucf_major_id", limit: 4
    t.string   "xcf_grade",     limit: 4000
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "xucf_colleges", force: :cascade do |t|
    t.string   "xcf_name",   limit: 4000
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "xucf_courses", force: :cascade do |t|
    t.string   "xcf_no",     limit: 4000, null: false
    t.string   "xcf_name",   limit: 4000, null: false
    t.float    "xcf_hour"
    t.boolean  "xcf_exam"
    t.float    "xcf_credit"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "xucf_majors", force: :cascade do |t|
    t.string   "xcf_name",        limit: 4000
    t.integer  "xucf_college_id", limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "xucf_reports", force: :cascade do |t|
    t.integer  "xucf_student_id", limit: 4, null: false
    t.integer  "xucf_course_id",  limit: 4, null: false
    t.float    "xcf_score"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "xucf_students", force: :cascade do |t|
    t.string   "xcf_no",         limit: 4000,               null: false
    t.string   "xcf_name",       limit: 4000,               null: false
    t.integer  "xucf_class_id",  limit: 4
    t.string   "xcf_gender",     limit: 4000
    t.string   "xcf_birthyear",  limit: 4000
    t.string   "xcf_birthplace", limit: 4000
    t.float    "xcf_credit",                  default: 0.0
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  add_index "xucf_students", ["xcf_name"], name: "index_xucf_students_on_xcf_name"

  create_table "xucf_teachers", force: :cascade do |t|
    t.string   "xcf_no",          limit: 4000, null: false
    t.string   "xcf_name",        limit: 4000, null: false
    t.integer  "xucf_college_id", limit: 4
    t.string   "xcf_gender",      limit: 4000
    t.string   "xcf_birthyear",   limit: 4000
    t.string   "xcf_title",       limit: 4000
    t.string   "xcf_tel",         limit: 4000
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "xucf_teachers", ["xcf_name"], name: "index_xucf_teachers_on_xcf_name"

end
