class CreateXucfTeachers < ActiveRecord::Migration
  def change
    create_table :xucf_teachers do |t|
      t.string :xcf_no, :null => false
      t.string :xcf_name, :null => false, :index => true
      t.integer :xcf_college_id
      t.string :xcf_gender
      t.string :xcf_birthyear
      t.string :xcf_title
      t.string :xcf_tel

      t.timestamps null: false
    end
  end
end
