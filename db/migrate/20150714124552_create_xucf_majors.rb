class CreateXucfMajors < ActiveRecord::Migration
  def change
    create_table :xucf_majors do |t|
      t.string :xcf_name
      t.integer :xcf_college_id

      t.timestamps null: false
    end
  end
end
