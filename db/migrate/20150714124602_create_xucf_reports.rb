class CreateXucfReports < ActiveRecord::Migration
  def change
    create_table :xucf_reports do |t|
      t.integer :xcf_student_id, :null => false
      t.integer :xcf_course_id, :null => false
      t.float :xcf_score

      t.timestamps null: false
    end
  end
end
