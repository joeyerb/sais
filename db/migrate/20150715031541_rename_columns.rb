class RenameColumns < ActiveRecord::Migration
  def change
    rename_column :xucf_reports, :xcf_course_id, :xucf_course_id
    rename_column :xucf_reports, :xcf_student_id, :xucf_student_id

    rename_column :xucf_class_courses, :xcf_class_id, :xucf_class_id
    rename_column :xucf_class_courses, :xcf_course_id, :xucf_course_id
    rename_column :xucf_class_courses, :xcf_teacher_id, :xucf_teacher_id

    rename_column :xucf_students, :xcf_class_id, :xucf_class_id

    rename_column :xucf_classes, :xcf_major_id, :xucf_major_id

    rename_column :xucf_majors, :xcf_college_id, :xucf_college_id

    rename_column :xucf_teachers, :xcf_college_id, :xucf_college_id
  end
end
