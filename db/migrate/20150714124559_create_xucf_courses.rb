class CreateXucfCourses < ActiveRecord::Migration
  def change
    create_table :xucf_courses do |t|
      t.string :xcf_no, :null => false
      t.string :xcf_name, :null => false
      t.float :xcf_hour
      t.boolean :xcf_exam
      t.float :xcf_credit

      t.timestamps null: false
    end
  end
end
