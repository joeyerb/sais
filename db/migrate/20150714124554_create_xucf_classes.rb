class CreateXucfClasses < ActiveRecord::Migration
  def change
    create_table :xucf_classes do |t|
      t.string :xcf_no
      t.integer :xcf_major_id
      t.string :xcf_grade

      t.timestamps null: false
    end
  end
end
