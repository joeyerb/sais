class CreateXucfStudents < ActiveRecord::Migration
  def change
    create_table :xucf_students do |t|
      t.string :xcf_no, :null => false
      t.string :xcf_name, :null => false, :index => true
      t.integer :xcf_class_id
      t.string :xcf_gender
      t.string :xcf_birthyear
      t.string :xcf_birthplace
      t.float :xcf_credit, :default => 0

      t.timestamps null: false
    end
  end
end
