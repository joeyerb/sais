class CreateXucfColleges < ActiveRecord::Migration
  def change
    create_table :xucf_colleges do |t|
      t.string :xcf_name

      t.timestamps null: false
    end
  end
end
