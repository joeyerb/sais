class CreateViewCourseScores < ActiveRecord::Migration
  def up
    execute <<-SQL
      CREATE VIEW xucf_course_scores AS
      SELECT xucf_reports.xucf_course_id, xucf_students.id AS student_id,
        xucf_students.xcf_no AS student_no, xucf_students.xcf_name AS student_name,
        xucf_students.xucf_class_id, xucf_courses.xcf_name AS course_name, xucf_reports.xcf_score
      FROM xucf_students, xucf_reports, xucf_courses
      WHERE xucf_courses.id = xucf_reports.xucf_course_id AND xucf_reports.xucf_student_id = xucf_students.id;
    SQL
  end

  def down
    execute 'DROP VIEW xucf_course_scores'
  end
end
