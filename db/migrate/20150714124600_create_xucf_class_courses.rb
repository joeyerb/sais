class CreateXucfClassCourses < ActiveRecord::Migration
  def change
    create_table :xucf_class_courses do |t|
      t.integer :xcf_class_id, :null => false
      t.integer :xcf_course_id, :null => false
      t.integer :xcf_teacher_id
      t.string :xcf_term

      t.timestamps null: false
    end
  end
end
