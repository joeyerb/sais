Rails.application.routes.draw do
  resources :reports, :as => :xucf_reports, :controller => :xucf_reports
  resources :class_courses, :as => :xucf_class_courses, :controller => :xucf_class_courses
  resources :courses, :as => :xucf_courses, :controller => :xucf_courses do
    member do
      get 'grade/:grade_id', :action => 'grade'
    end
  end
  resources :teachers, :as => :xucf_teachers, :controller => :xucf_teachers do
    member do
      get :courses
    end
  end
  resources :classes, :as => :xucf_classes, :controller => :xucf_classes do
    resources :students, :as => :xucf_students, :controller => :xucf_students do
      member do
        get :scores
        get :courses
        get 'term/:term_id', :action => 'term'
      end
    end
    member do
      get :courses
    end
  end
  resources :majors, :as => :xucf_majors, :controller => :xucf_majors do
    member do
      get 'rank/:grade_id', :action => 'rank'
    end
  end
  #resources :xucf_colleges
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
